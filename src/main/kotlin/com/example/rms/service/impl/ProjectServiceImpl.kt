package com.example.rms.service.impl

import com.example.rms.database.dao.ProjectDao
import com.example.rms.database.dao.UserDao
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.project.ProjectRequest
import com.example.rms.model.dto.project.ProjectResponse
import com.example.rms.model.dto.project.ProjectShortResponse
import com.example.rms.model.exception.common.ResourceNotFoundException
import com.example.rms.model.mapper.ProjectMapper
import com.example.rms.service.ProjectService
import org.springframework.data.jpa.repository.Modifying
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class ProjectServiceImpl(
    private val userDao: UserDao,
    private val projectDao: ProjectDao,
    private val mapper: ProjectMapper
) : ProjectService {

    override fun list(query: String, request: PageRequest): PageResponse<ProjectShortResponse> {
        val projects = projectDao.findByNameStartsWith(query, request.pageable)
        return mapper.asPageResponse(projects)
    }

    override fun listByUserId(userId: Long, query: String, request: PageRequest): PageResponse<ProjectShortResponse> {
        val user = userDao.findUserById(userId) ?: throw ResourceNotFoundException()
        val projects = projectDao.findByUsersContainsAndNameStartsWith(user, query, request.pageable)
        return mapper.asPageResponse(projects)
    }

    override fun create(request: ProjectRequest): ProjectResponse {
        val entity = mapper.asEntity(request)
        val project = projectDao.save(entity)
        return mapper.asResponse(project)
    }

    override fun findById(id: Long): ProjectResponse {
        val project = projectDao.findProjectById(id) ?: throw ResourceNotFoundException()
        return mapper.asResponse(project)
    }

    override fun findByUserIdAndId(userId: Long, id: Long): ProjectResponse {
        val user = userDao.findUserById(userId) ?: throw ResourceNotFoundException()
        val project = projectDao.findByUsersContainsAndId(user, id) ?: throw ResourceNotFoundException()
        return mapper.asResponse(project)
    }

    @Modifying
    override fun update(id: Long, request: ProjectRequest): ProjectResponse {
        val project = projectDao.findProjectById(id) ?: throw ResourceNotFoundException()
        val updated = mapper.update(project, request)
        return mapper.asResponse(updated)
    }

    @Modifying
    override fun delete(id: Long) {
        val project = projectDao.findProjectById(id) ?: throw ResourceNotFoundException()
        projectDao.delete(project)
    }
}