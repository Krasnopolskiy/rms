package com.example.rms.service.impl

import com.example.rms.database.dao.ProjectDao
import com.example.rms.database.dao.ReleaseDao
import com.example.rms.database.dao.RequirementDao
import com.example.rms.database.dao.UserDao
import com.example.rms.model.exception.common.ResourceNotFoundException
import com.example.rms.service.PermissionService
import org.springframework.stereotype.Service

@Service
class PermissionServiceImpl(
    private val userDao: UserDao,
    private val projectDao: ProjectDao,
    private val releaseDao: ReleaseDao,
    private val requirementDao: RequirementDao
) : PermissionService {
    override fun assertCanAccessProject(userId: Long, projectId: Long) {
        val user = userDao.findUserById(userId) ?: throw ResourceNotFoundException()
        if (!projectDao.existsByUsersContainsAndId(user, projectId))
            throw ResourceNotFoundException() // or ForbiddenException()
    }

    override fun assertCanAccessRelease(userId: Long, releaseId: Long) {
        val release = releaseDao.findReleaseById(releaseId) ?: throw ResourceNotFoundException()
        return assertCanAccessProject(userId, release.project.id)
    }

    override fun assertCanAccessRequirement(userId: Long, requirementId: Long) {
        val requirement = requirementDao.findRequirementById(requirementId) ?: throw ResourceNotFoundException()
        return assertCanAccessProject(userId, requirement.release.project.id)
    }
}