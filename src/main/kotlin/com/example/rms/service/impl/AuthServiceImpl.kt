package com.example.rms.service.impl

import com.example.rms.database.dao.UserDao
import com.example.rms.model.dto.auth.LoginRequest
import com.example.rms.model.dto.auth.TokenResponse
import com.example.rms.model.exception.auth.InvalidCredentialsException
import com.example.rms.service.AuthService
import com.example.rms.util.security.JwtManager
import com.example.rms.util.security.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class AuthServiceImpl(
    private val encoder: PasswordEncoder,
    private val manager: JwtManager,
    private val dao: UserDao
) : AuthService {
    override fun login(request: LoginRequest): TokenResponse {
        val user = dao.findByLogin(request.login) ?: throw InvalidCredentialsException()
        if (!encoder.matches(request.password, user.hash))
            throw InvalidCredentialsException()
        val token = manager.generateToken(user)
        return TokenResponse(token)
    }
}