package com.example.rms.service.impl

import com.example.rms.database.dao.UserDao
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.user.UserRequest
import com.example.rms.model.dto.user.UserResponse
import com.example.rms.model.exception.common.ResourceNotFoundException
import com.example.rms.model.exception.user.AdminDeletionException
import com.example.rms.model.exception.user.LoginAlreadyExistsException
import com.example.rms.model.mapper.UserMapper
import com.example.rms.service.UserService
import org.springframework.data.jpa.repository.Modifying
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class UserServiceImpl(
    private val mapper: UserMapper,
    private val dao: UserDao
) : UserService {
    override fun list(query: String, request: PageRequest): PageResponse<UserResponse> {
        val users = dao.findByLoginStartsWith(query, request.pageable)
        return mapper.asPageResponse(users)
    }

    override fun create(request: UserRequest, admin: Boolean): UserResponse {
        if (existsByLogin(request.login))
            throw LoginAlreadyExistsException()
        val entity = mapper.asEntity(request, admin)
        val user = dao.save(entity)
        return mapper.asResponse(user)
    }

    override fun findById(id: Long): UserResponse {
        val user = dao.findUserById(id) ?: throw ResourceNotFoundException()
        return mapper.asResponse(user)
    }

    @Modifying
    override fun delete(id: Long) {
        val user = dao.findUserById(id) ?: throw ResourceNotFoundException()
        if (user.admin) throw AdminDeletionException()
        dao.delete(user)
    }

    override fun existsByLogin(login: String) = dao.existsByLogin(login)
}