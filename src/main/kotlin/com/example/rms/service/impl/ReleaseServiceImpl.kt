package com.example.rms.service.impl

import com.example.rms.database.dao.ReleaseDao
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.release.ReleaseRequest
import com.example.rms.model.dto.release.ReleaseResponse
import com.example.rms.model.dto.release.ReleaseShortResponse
import com.example.rms.model.exception.common.ResourceNotFoundException
import com.example.rms.model.mapper.ReleaseMapper
import com.example.rms.service.ReleaseService
import org.springframework.data.jpa.repository.Modifying
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class ReleaseServiceImpl(
    private val dao: ReleaseDao,
    private val mapper: ReleaseMapper
) : ReleaseService {
    override fun list(projectId: Long, query: String, request: PageRequest): PageResponse<ReleaseShortResponse> {
        val releases = dao.findByProjectIdAndNameStartsWith(projectId, query, request.pageable)
        return mapper.asPageResponse(releases)
    }

    override fun create(projectId: Long, request: ReleaseRequest): ReleaseResponse {
        val entity = mapper.asEntity(projectId, request)
        val release = dao.save(entity)
        return mapper.asResponse(release)
    }

    override fun findById(id: Long): ReleaseResponse {
        val release = dao.findReleaseById(id) ?: throw ResourceNotFoundException()
        return mapper.asResponse(release)
    }

    @Modifying
    override fun update(id: Long, request: ReleaseRequest): ReleaseResponse {
        val release = dao.findReleaseById(id) ?: throw ResourceNotFoundException()
        val updated = mapper.update(release, request)
        return mapper.asResponse(updated)
    }

    @Modifying
    override fun delete(id: Long) {
        val release = dao.findReleaseById(id) ?: throw ResourceNotFoundException()
        dao.delete(release)
    }
}