package com.example.rms.service.impl

import com.example.rms.database.dao.RequirementDao
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.requirement.RequirementRequest
import com.example.rms.model.dto.requirement.RequirementResponse
import com.example.rms.model.dto.requirement.RequirementShortResponse
import com.example.rms.model.dto.requirement.StateRequest
import com.example.rms.model.exception.common.ResourceNotFoundException
import com.example.rms.model.mapper.RequirementMapper
import com.example.rms.service.RequirementService
import org.springframework.data.jpa.repository.Modifying
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class RequirementServiceImpl(
    private val dao: RequirementDao,
    private val mapper: RequirementMapper
) : RequirementService {
    override fun list(releaseId: Long, query: String, request: PageRequest): PageResponse<RequirementShortResponse> {
        val requirements = dao.findByReleaseIdAndNameStartsWith(releaseId, query, request.pageable)
        return mapper.asPageResponse(requirements)
    }

    override fun create(releaseId: Long, request: RequirementRequest): RequirementResponse {
        val entity = mapper.asEntity(releaseId, request)
        val requirement = dao.save(entity)
        return mapper.asResponse(requirement)
    }

    override fun findById(id: Long): RequirementResponse {
        val requirement = dao.findRequirementById(id) ?: throw ResourceNotFoundException()
        return mapper.asResponse(requirement)
    }

    @Modifying
    override fun update(id: Long, request: RequirementRequest): RequirementResponse {
        val requirement = dao.findRequirementById(id) ?: throw ResourceNotFoundException()
        val updated = mapper.update(requirement, request)
        return mapper.asResponse(updated)
    }

    @Modifying
    override fun update(id: Long, request: StateRequest): RequirementResponse {
        val requirement = dao.findRequirementById(id) ?: throw ResourceNotFoundException()
        val updated = mapper.update(requirement, request)
        return mapper.asResponse(updated)
    }

    @Modifying
    override fun delete(id: Long) {
        val requirement = dao.findRequirementById(id) ?: throw ResourceNotFoundException()
        dao.delete(requirement)
    }
}