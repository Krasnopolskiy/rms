package com.example.rms.service

import com.example.rms.model.dto.auth.LoginRequest
import com.example.rms.model.dto.auth.TokenResponse

interface AuthService {
    fun login(request: LoginRequest): TokenResponse
}