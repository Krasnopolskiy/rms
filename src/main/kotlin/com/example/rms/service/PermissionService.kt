package com.example.rms.service

interface PermissionService {
    fun assertCanAccessProject(userId: Long, projectId: Long)
    fun assertCanAccessRelease(userId: Long, releaseId: Long)
    fun assertCanAccessRequirement(userId: Long, requirementId: Long)
}