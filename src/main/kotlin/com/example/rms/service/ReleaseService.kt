package com.example.rms.service

import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.release.ReleaseRequest
import com.example.rms.model.dto.release.ReleaseResponse
import com.example.rms.model.dto.release.ReleaseShortResponse

interface ReleaseService {
    fun list(projectId: Long, query: String, request: PageRequest): PageResponse<ReleaseShortResponse>
    fun create(projectId: Long, request: ReleaseRequest): ReleaseResponse
    fun findById(id: Long): ReleaseResponse
    fun update(id: Long, request: ReleaseRequest): ReleaseResponse
    fun delete(id: Long)
}