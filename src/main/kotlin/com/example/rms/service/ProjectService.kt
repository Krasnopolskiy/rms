package com.example.rms.service

import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.project.ProjectRequest
import com.example.rms.model.dto.project.ProjectResponse
import com.example.rms.model.dto.project.ProjectShortResponse

interface ProjectService {
    fun list(query: String, request: PageRequest): PageResponse<ProjectShortResponse>
    fun listByUserId(userId: Long, query: String, request: PageRequest): PageResponse<ProjectShortResponse>
    fun create(request: ProjectRequest): ProjectResponse
    fun findById(id: Long): ProjectResponse
    fun findByUserIdAndId(userId: Long, id: Long): ProjectResponse
    fun update(id: Long, request: ProjectRequest): ProjectResponse
    fun delete(id: Long)
}