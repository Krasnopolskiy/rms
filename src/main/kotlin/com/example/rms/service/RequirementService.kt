package com.example.rms.service

import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.requirement.RequirementRequest
import com.example.rms.model.dto.requirement.RequirementResponse
import com.example.rms.model.dto.requirement.RequirementShortResponse
import com.example.rms.model.dto.requirement.StateRequest

interface RequirementService {
    fun list(releaseId: Long, query: String, request: PageRequest): PageResponse<RequirementShortResponse>
    fun create(releaseId: Long, request: RequirementRequest): RequirementResponse
    fun findById(id: Long): RequirementResponse
    fun update(id: Long, request: RequirementRequest): RequirementResponse
    fun update(id: Long, request: StateRequest): RequirementResponse
    fun delete(id: Long)
}