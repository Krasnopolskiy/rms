package com.example.rms.service

import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.user.UserRequest
import com.example.rms.model.dto.user.UserResponse

interface UserService {
    fun list(query: String, request: PageRequest): PageResponse<UserResponse>
    fun create(request: UserRequest, admin: Boolean = false): UserResponse
    fun findById(id: Long): UserResponse
    fun delete(id: Long)
    fun existsByLogin(login: String): Boolean
}