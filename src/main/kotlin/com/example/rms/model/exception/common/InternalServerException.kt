package com.example.rms.model.exception.common

import com.example.rms.model.exception.AbstractApiException
import org.springframework.http.HttpStatus

class InternalServerException : AbstractApiException(
    code = HttpStatus.INTERNAL_SERVER_ERROR,
    message = "Что-то пошло не так"
)
