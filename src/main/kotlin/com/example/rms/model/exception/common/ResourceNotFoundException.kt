package com.example.rms.model.exception.common

import com.example.rms.model.exception.AbstractApiException
import org.springframework.http.HttpStatus

class ResourceNotFoundException : AbstractApiException(
    code = HttpStatus.NOT_FOUND,
    message = "Ресурс не найден"
)
