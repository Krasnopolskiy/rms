package com.example.rms.model.exception

import com.example.rms.model.dto.ApiResponse
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.http.HttpStatus

@JsonIgnoreProperties("cause", "stackTrace", "suppressed", "localizedMessage")
abstract class AbstractApiException(
    override val message: String,

    @JsonIgnore
    val code: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR
) : ApiResponse, Exception() {
    override fun status() = code
}
