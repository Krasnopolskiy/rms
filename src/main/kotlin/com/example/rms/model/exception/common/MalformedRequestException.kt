package com.example.rms.model.exception.common

import com.example.rms.model.exception.AbstractApiException
import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.http.HttpStatus

class MalformedRequestException(
    @JsonInclude(JsonInclude.Include.NON_NULL)
    val reason: String? = null
) : AbstractApiException(
    code = HttpStatus.BAD_REQUEST,
    message = "Некорректный запрос"
)
