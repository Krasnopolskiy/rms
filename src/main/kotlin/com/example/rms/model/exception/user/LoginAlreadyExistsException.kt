package com.example.rms.model.exception.user

import com.example.rms.model.exception.AbstractApiException
import org.springframework.http.HttpStatus

class LoginAlreadyExistsException : AbstractApiException(
    code = HttpStatus.CONFLICT,
    message = "Логин уже используется"
)