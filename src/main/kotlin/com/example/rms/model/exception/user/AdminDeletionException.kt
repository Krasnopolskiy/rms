package com.example.rms.model.exception.user

import com.example.rms.model.exception.AbstractApiException
import org.springframework.http.HttpStatus

class AdminDeletionException : AbstractApiException(
    code = HttpStatus.FORBIDDEN,
    message = "Нельзя удалить администратора"
)