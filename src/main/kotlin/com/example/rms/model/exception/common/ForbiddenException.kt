package com.example.rms.model.exception.common

import com.example.rms.model.exception.AbstractApiException
import org.springframework.http.HttpStatus

class ForbiddenException : AbstractApiException(
    code = HttpStatus.FORBIDDEN,
    message = "Доступ запрещен"
)
