package com.example.rms.model.exception.auth

import com.example.rms.model.exception.AbstractApiException
import org.springframework.http.HttpStatus

class InvalidCredentialsException : AbstractApiException(
    code = HttpStatus.UNAUTHORIZED,
    message = "Неверный логин или пароль"
)