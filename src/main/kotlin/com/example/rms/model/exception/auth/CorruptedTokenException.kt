package com.example.rms.model.exception.auth

import com.example.rms.model.exception.AbstractApiException
import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.http.HttpStatus

class CorruptedTokenException(
    @JsonInclude(JsonInclude.Include.NON_NULL)
    val reason: String? = null
) : AbstractApiException(
    code = HttpStatus.UNAUTHORIZED,
    message = "Токен поврежден"
)