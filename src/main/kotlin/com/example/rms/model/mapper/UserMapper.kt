package com.example.rms.model.mapper

import com.example.rms.database.entity.User
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.user.UserRequest
import com.example.rms.model.dto.user.UserResponse
import com.example.rms.util.security.PasswordEncoder
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class UserMapper(
    private val encoder: PasswordEncoder
) {

    fun asEntity(request: UserRequest, admin: Boolean = false): User {
        return User(
            login = request.login,
            hash = encoder.encode(request.password)
        ).apply { this.admin = admin }
    }

    fun asResponse(user: User): UserResponse {
        return UserResponse(
            id = user.id,
            createdAt = user.createdAt,
            login = user.login,
            admin = user.admin
        )
    }

    fun asPageResponse(users: Page<User>) = PageResponse(users.map(::asResponse))
}