package com.example.rms.model.mapper

import com.example.rms.database.dao.ProjectDao
import com.example.rms.database.entity.Release
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.release.ReleaseRequest
import com.example.rms.model.dto.release.ReleaseResponse
import com.example.rms.model.dto.release.ReleaseShortResponse
import com.example.rms.model.exception.common.ResourceNotFoundException
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class ReleaseMapper(
    private val projectDao: ProjectDao,
    private val projectMapper: ProjectMapper
) {
    fun asEntity(projectId: Long, request: ReleaseRequest): Release {
        return Release(
            name = request.name,
            project = projectDao.findProjectById(projectId) ?: throw ResourceNotFoundException()
        )
    }

    fun update(release: Release, request: ReleaseRequest): Release {
        release.name = request.name
        return release
    }

    fun asResponse(release: Release): ReleaseResponse {
        return ReleaseResponse(
            id = release.id,
            createdAt = release.createdAt,
            name = release.name,
            project = projectMapper.asShortResponse(release.project)
        )
    }

    fun asShortResponse(release: Release): ReleaseShortResponse {
        return ReleaseShortResponse(
            id = release.id,
            createdAt = release.createdAt,
            name = release.name
        )
    }

    fun asPageResponse(releases: Page<Release>) = PageResponse(releases.map(::asShortResponse))
}