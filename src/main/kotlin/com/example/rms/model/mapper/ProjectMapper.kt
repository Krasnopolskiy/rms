package com.example.rms.model.mapper

import com.example.rms.database.dao.UserDao
import com.example.rms.database.entity.Project
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.project.ProjectRequest
import com.example.rms.model.dto.project.ProjectResponse
import com.example.rms.model.dto.project.ProjectShortResponse
import com.example.rms.model.exception.common.ResourceNotFoundException
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class ProjectMapper(
    private val userDao: UserDao,
    private val userMapper: UserMapper
) {
    fun asEntity(request: ProjectRequest): Project {
        return Project(
            name = request.name,
            users = findUsersOrThrow(request.users)
        )
    }

    fun update(project: Project, request: ProjectRequest): Project {
        project.name = request.name
        project.users = findUsersOrThrow(request.users)
        return project
    }

    fun asResponse(project: Project): ProjectResponse {
        return ProjectResponse(
            id = project.id,
            createdAt = project.createdAt,
            name = project.name,
            users = project.users.map { userMapper.asResponse(it) }
        )
    }

    fun asShortResponse(project: Project): ProjectShortResponse {
        return ProjectShortResponse(
            id = project.id,
            createdAt = project.createdAt,
            name = project.name,
            users = project.users.map { userMapper.asResponse(it) }
        )
    }

    fun asPageResponse(projects: Page<Project>) = PageResponse(projects.map(::asShortResponse))

    private fun findUsersOrThrow(ids: List<Long>) =
        ids.map { userDao.findUserById(it) ?: throw ResourceNotFoundException() }
}