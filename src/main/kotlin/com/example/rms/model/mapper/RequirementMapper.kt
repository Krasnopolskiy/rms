package com.example.rms.model.mapper

import com.example.rms.database.dao.ReleaseDao
import com.example.rms.database.dao.RequirementDao
import com.example.rms.database.entity.Requirement
import com.example.rms.model.dto.common.PageResponse
import com.example.rms.model.dto.requirement.*
import com.example.rms.model.exception.common.ResourceNotFoundException
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class RequirementMapper(
    private val releaseMapper: ReleaseMapper,
    private val requirementDao: RequirementDao,
    private val releaseDao: ReleaseDao
) {
    fun asEntity(releaseId: Long, request: RequirementRequest): Requirement {
        return Requirement(
            name = request.name,
            deps = findRequirementsOrThrow(request.deps),
            release = releaseDao.findReleaseById(releaseId) ?: throw ResourceNotFoundException()
        )
    }

    fun update(requirement: Requirement, request: RequirementRequest): Requirement {
        requirement.name = request.name
        requirement.deps = findRequirementsOrThrow(request.deps)
        return requirement
    }

    fun update(requirement: Requirement, request: StateRequest): Requirement {
        requirement.state = request.state
        return requirement
    }

    fun asResponse(requirement: Requirement): RequirementResponse {
        return RequirementResponse(
            id = requirement.id,
            createdAt = requirement.createdAt,
            name = requirement.name,
            state = requirement.state,
            release = releaseMapper.asShortResponse(requirement.release),
            deps = mergeDependencies(requirement).map(::asDependencyResponse)
        )
    }

    fun asShortResponse(requirement: Requirement): RequirementShortResponse {
        return RequirementShortResponse(
            id = requirement.id,
            createdAt = requirement.createdAt,
            name = requirement.name,
            state = requirement.state,
            deps = mergeDependencies(requirement).map(::asDependencyResponse)
        )
    }

    fun asDependencyResponse(requirement: Requirement): DependencyResponse {
        return DependencyResponse(
            id = requirement.id,
            createdAt = requirement.createdAt,
            name = requirement.name
        )
    }

    fun asPageResponse(requirements: Page<Requirement>) = PageResponse(requirements.map(::asShortResponse))

    private fun findRequirementsOrThrow(ids: List<Long>) =
        ids.map { requirementDao.findRequirementById(it) ?: throw ResourceNotFoundException() }

    private fun mergeDependencies(requirement: Requirement) =
        (requirement.deps + requirement.reqs).distinctBy { it.id }.sortedBy { it.id }
}