package com.example.rms.model.dto.requirement

import com.example.rms.model.dto.MessageResponse

class RequirementUpdatedMessage(override val data: RequirementResponse) : MessageResponse<RequirementResponse> {
    override val message: String = "Требование успешно обновлено"
}