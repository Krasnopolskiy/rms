package com.example.rms.model.dto.release

import com.example.rms.model.dto.EntityResponse
import java.time.LocalDateTime

class ReleaseShortResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val name: String
) : EntityResponse