package com.example.rms.model.dto.release

import com.example.rms.model.dto.MessageResponse

class ReleaseUpdatedMessage(override val data: ReleaseResponse) : MessageResponse<ReleaseResponse> {
    override val message: String = "Релиз успешно обновлен"
}