package com.example.rms.model.dto.auth

import com.example.rms.model.dto.MessageResponse

class AuthenticatedMessage(override val data: TokenResponse) : MessageResponse<TokenResponse> {
    override val message: String = "Успешная аутентификация"
}