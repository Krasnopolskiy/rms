package com.example.rms.model.dto.requirement

import com.example.rms.model.dto.MessageResponse

class RequirementCreatedMessage(override val data: RequirementResponse) : MessageResponse<RequirementResponse> {
    override val message: String = "Требование успешно создано"
}