package com.example.rms.model.dto

import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

interface MessageResponse<T> {
    val message: String

    @get:JsonInclude(JsonInclude.Include.NON_NULL)
    val data: T?
    fun status() = HttpStatus.OK
    fun asResponse() = ResponseEntity.status(status()).body(this)
}
