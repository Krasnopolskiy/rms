package com.example.rms.model.dto.release

import com.example.rms.model.dto.EntityResponse
import com.example.rms.model.dto.project.ProjectShortResponse
import java.time.LocalDateTime

class ReleaseResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val name: String,
    val project: ProjectShortResponse,
) : EntityResponse