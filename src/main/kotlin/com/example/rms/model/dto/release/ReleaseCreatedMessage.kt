package com.example.rms.model.dto.release

import com.example.rms.model.dto.MessageResponse

class ReleaseCreatedMessage(override val data: ReleaseResponse) : MessageResponse<ReleaseResponse> {
    override val message: String = "Релиз успешно создан"
}