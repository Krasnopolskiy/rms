package com.example.rms.model.dto

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

interface ApiResponse {
    fun status() = HttpStatus.OK
    fun asResponse() = ResponseEntity.status(status()).body(this)
}
