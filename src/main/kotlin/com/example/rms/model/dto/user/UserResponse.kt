package com.example.rms.model.dto.user

import com.example.rms.model.dto.EntityResponse
import java.time.LocalDateTime

class UserResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val login: String,
    val admin: Boolean
) : EntityResponse
