package com.example.rms.model.dto.common

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

class PageRequest(
    page: Int = 1,
    size: Int = 20,
    order: Sort.Direction = Sort.Direction.DESC,
    field: String = "id"
) {
    var pageable = PageRequest.of(page - 1, size, order, field)
}