package com.example.rms.model.dto.requirement

import com.example.rms.model.dto.EntityResponse
import java.time.LocalDateTime

class DependencyResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val name: String
) : EntityResponse