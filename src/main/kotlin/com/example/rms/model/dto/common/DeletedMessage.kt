package com.example.rms.model.dto.common

import com.example.rms.model.dto.MessageResponse

class DeletedMessage : MessageResponse<Any> {
    override val data: Any? = null
    override val message = "Успешно удалено"
}