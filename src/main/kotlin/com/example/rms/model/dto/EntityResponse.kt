package com.example.rms.model.dto

import java.time.LocalDateTime

interface EntityResponse : ApiResponse {
    val id: Long
    val createdAt: LocalDateTime
}