package com.example.rms.model.dto.project

import com.example.rms.model.dto.MessageResponse

class ProjectCreatedMessage(override val data: ProjectResponse) : MessageResponse<ProjectResponse> {
    override val message: String = "Проект успешно создан"
}