package com.example.rms.model.dto.project

import com.example.rms.util.constant.LengthConstants
import javax.validation.constraints.Size

class ProjectRequest(
    @field:Size(max = LengthConstants.CHAR_FIELD)
    val name: String,

    val users: List<Long>
)