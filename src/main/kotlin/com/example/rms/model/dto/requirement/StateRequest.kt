package com.example.rms.model.dto.requirement

import com.example.rms.database.entity.Requirement

class StateRequest(
    val state: Requirement.State
)