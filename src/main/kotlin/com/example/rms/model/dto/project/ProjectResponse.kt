package com.example.rms.model.dto.project

import com.example.rms.model.dto.EntityResponse
import com.example.rms.model.dto.user.UserResponse
import java.time.LocalDateTime

class ProjectResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val name: String,
    val users: List<UserResponse>
) : EntityResponse