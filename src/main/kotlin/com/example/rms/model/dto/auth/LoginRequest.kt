package com.example.rms.model.dto.auth

import com.example.rms.util.constant.LengthConstants
import javax.validation.constraints.Size

class LoginRequest(
    @field:Size(max = LengthConstants.CHAR_FIELD)
    val login: String,

    @field:Size(min = 3, max = LengthConstants.CHAR_FIELD)
    val password: String
)