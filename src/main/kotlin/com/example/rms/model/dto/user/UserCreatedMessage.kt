package com.example.rms.model.dto.user

import com.example.rms.model.dto.MessageResponse

class UserCreatedMessage(override val data: UserResponse) : MessageResponse<UserResponse> {
    override val message: String = "Пользователь успешно создан"
}