package com.example.rms.model.dto.requirement

import com.example.rms.database.entity.Requirement
import com.example.rms.model.dto.EntityResponse
import com.example.rms.model.dto.release.ReleaseShortResponse
import java.time.LocalDateTime

class RequirementResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val name: String,
    val state: Requirement.State,
    val release: ReleaseShortResponse,
    val deps: List<DependencyResponse>
) : EntityResponse