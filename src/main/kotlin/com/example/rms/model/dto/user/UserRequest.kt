package com.example.rms.model.dto.user

import com.example.rms.util.constant.LengthConstants
import javax.validation.constraints.Size

class UserRequest(
    @field:Size(max = LengthConstants.CHAR_FIELD)
    val login: String,

    @field:Size(max = LengthConstants.CHAR_FIELD)
    val password: String
)