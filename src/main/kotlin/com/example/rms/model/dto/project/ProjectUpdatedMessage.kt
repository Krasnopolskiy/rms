package com.example.rms.model.dto.project

import com.example.rms.model.dto.MessageResponse

class ProjectUpdatedMessage(override val data: ProjectResponse) : MessageResponse<ProjectResponse> {
    override val message: String = "Проект успешно обновлен"
}