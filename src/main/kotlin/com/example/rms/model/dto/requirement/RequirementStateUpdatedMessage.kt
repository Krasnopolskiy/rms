package com.example.rms.model.dto.requirement

import com.example.rms.model.dto.MessageResponse

class RequirementStateUpdatedMessage(override val data: RequirementResponse) : MessageResponse<RequirementResponse> {
    override val message: String = "Состояние требования успешно обновлено"
}