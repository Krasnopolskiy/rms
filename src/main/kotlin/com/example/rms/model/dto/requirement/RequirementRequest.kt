package com.example.rms.model.dto.requirement

import com.example.rms.util.constant.LengthConstants
import javax.validation.constraints.Size

class RequirementRequest(
    @field:Size(max = LengthConstants.CHAR_FIELD)
    val name: String,
    val deps: List<Long>
)