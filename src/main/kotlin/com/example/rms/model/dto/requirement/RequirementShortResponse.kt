package com.example.rms.model.dto.requirement

import com.example.rms.database.entity.Requirement
import com.example.rms.model.dto.EntityResponse
import java.time.LocalDateTime

class RequirementShortResponse(
    override val id: Long,
    override val createdAt: LocalDateTime,
    val name: String,
    val state: Requirement.State,
    val deps: List<DependencyResponse>
) : EntityResponse