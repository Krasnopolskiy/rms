package com.example.rms.model.dto.auth

import com.example.rms.model.dto.ApiResponse

class TokenResponse(
    val token: String
) : ApiResponse