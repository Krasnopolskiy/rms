package com.example.rms.model.dto.release

import com.example.rms.util.constant.LengthConstants
import javax.validation.constraints.Size

class ReleaseRequest(
    @field:Size(max = LengthConstants.CHAR_FIELD)
    val name: String
)