@file:Suppress("unused")

package com.example.rms.model.dto.common

import com.example.rms.model.dto.ApiResponse
import org.springframework.data.domain.Page

@Suppress("unused")
class PageResponse<T : ApiResponse>(

    number: Long,
    numberOfElements: Long,
    val totalPages: Long,
    val content: List<T>

) : ApiResponse {
    val page = number + 1
    val size = numberOfElements

    constructor (page: Page<T>) : this(
        content = page.content,
        number = page.number.toLong(),
        numberOfElements = page.numberOfElements.toLong(),
        totalPages = page.totalPages.toLong()
    )
}