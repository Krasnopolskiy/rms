package com.example.rms.util.initializer

import com.example.rms.model.dto.user.UserRequest
import com.example.rms.service.UserService
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class AdminInitializer(
    @Value("\${spring.admin.user}")
    private val login: String,

    @Value("\${spring.admin.password}")
    private val password: String,

    private val service: UserService
) {
    @PostConstruct
    fun saveAdmin() {
        if (service.existsByLogin(login)) return
        val request = UserRequest(login = login, password = password)
        service.create(request, admin = true)
    }
}