package com.example.rms.util.config

import com.example.rms.util.filter.JwtFilter
import com.example.rms.util.security.CustomAuthenticationProvider
import com.example.rms.util.security.ForbiddenHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@Configuration
@EnableWebSecurity
class SecurityConfig(
    private val jwtFilter: JwtFilter,
    private val forbiddenHandler: ForbiddenHandler,
    private val authProvider: CustomAuthenticationProvider
) {
    @Bean
    fun configure(http: HttpSecurity): SecurityFilterChain {
        http.csrf().disable()

        http.authorizeRequests()
            .antMatchers("/auth/login").permitAll()
            .antMatchers("/admin/**").hasRole("ADMIN")
            .anyRequest().authenticated()

        http.httpBasic()
            .and().authenticationProvider(authProvider)
            .exceptionHandling().accessDeniedHandler(forbiddenHandler)

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter::class.java)

        return http.build()
    }
}