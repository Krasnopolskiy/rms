package com.example.rms.util.filter

import com.example.rms.model.exception.AbstractApiException
import com.example.rms.model.exception.auth.CorruptedTokenException
import com.example.rms.util.interceptor.ExceptionResolver
import com.example.rms.util.security.AuthenticationToken
import com.example.rms.util.security.JwtManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtFilter(
    private val manager: JwtManager,
    private val resolver: ExceptionResolver
) : OncePerRequestFilter() {
    private val prefix = "Bearer "

    override fun shouldNotFilter(request: HttpServletRequest): Boolean {
        return request.requestURI.contains("/login")
    }

    override fun doFilterInternal(
        request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain
    ) {
        try {
            val header = request.getHeader("Authorization") ?: throw CorruptedTokenException()
            SecurityContextHolder.getContext().authentication = parseHeader(header)
            filterChain.doFilter(request, response)
        } catch (exception: AbstractApiException) {
            resolver.resolve(request, response, exception)
        }
    }

    private fun parseHeader(header: String): AuthenticationToken {
        val token = header.replace(prefix, "")
        return manager.parseToken(token)
    }
}