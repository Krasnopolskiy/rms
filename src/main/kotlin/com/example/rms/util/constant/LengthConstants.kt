package com.example.rms.util.constant

sealed class LengthConstants {
    companion object {
        const val CHAR_FIELD = 256
        const val HASH = 72
    }
}