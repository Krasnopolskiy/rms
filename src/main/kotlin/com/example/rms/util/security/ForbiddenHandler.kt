package com.example.rms.util.security

import com.example.rms.model.exception.common.ForbiddenException
import com.example.rms.util.interceptor.ExceptionResolver
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class ForbiddenHandler(
    private var resolver: ExceptionResolver
) : AccessDeniedHandler {
    override fun handle(
        request: HttpServletRequest, response: HttpServletResponse, accessDeniedException: AccessDeniedException
    ) {
        val exception = ForbiddenException()
        resolver.resolve(request, response, exception)
    }
}