package com.example.rms.util.security

import com.example.rms.database.entity.User
import com.example.rms.model.exception.auth.CorruptedTokenException
import io.jsonwebtoken.*
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

@Component
class JwtManager(
    @Value("\${spring.security.secret}")
    private val secret: String,
    @Value("\${spring.security.token.lifetime}")
    private val lifetime: Long
) {
    private val algorithm = SignatureAlgorithm.HS256
    private val key = Keys.hmacShaKeyFor(secret.toByteArray())

    fun generateToken(user: User): String {
        val expiration = getAccessTokenExpiration()
        val authorities = mutableListOf(Authority.USER)
        if (user.admin) authorities.add(Authority.ADMIN)
        return Jwts.builder()
            .claim("userId", user.id)
            .claim("authorities", authorities)
            .setExpiration(expiration)
            .signWith(key, algorithm).compact()
    }

    fun parseToken(token: String): AuthenticationToken {
        val claims = parseTokenClaims(token)
        return createAuthenticationToken(claims)
    }

    private fun createAuthenticationToken(claims: Jws<Claims>): AuthenticationToken {
        try {
            val userId = claims.body["userId"] as Int
            val authorities = (claims.body["authorities"] as List<*>).map { Authority.valueOf(it as String) }
            return AuthenticationToken(userId, authorities)
        } catch (e: Throwable) {
            throw CorruptedTokenException()
        }
    }

    private fun parseTokenClaims(token: String): Jws<Claims> {
        return try {
            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token)
        } catch (e: JwtException) {
            throw CorruptedTokenException(e.message)
        }
    }

    private fun getAccessTokenExpiration(): Date =
        Instant.now().plus(lifetime, ChronoUnit.DAYS).let { Date.from(it) }
}