package com.example.rms.util.security

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component
class PasswordEncoder {
    val encoder = BCryptPasswordEncoder()

    fun encode(password: String): String = encoder.encode(password)

    fun matches(password: String, encoded: String) = encoder.matches(password, encoded)
}