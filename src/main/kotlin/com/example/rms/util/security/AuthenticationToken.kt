package com.example.rms.util.security

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class AuthenticationToken(
    private val principal: Long,
    authorities: List<GrantedAuthority>
) : AbstractAuthenticationToken(authorities) {

    constructor(userId: Int, authorities: List<Authority>) :
            this(userId.toLong(), authorities.map { it.grantedAuthority })

    override fun getCredentials() = null

    override fun getPrincipal() = principal
}