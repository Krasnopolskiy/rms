package com.example.rms.util.security

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Component
class CustomAuthenticationProvider : AuthenticationProvider {
    override fun authenticate(authentication: Authentication) = authentication

    override fun supports(authentication: Class<*>?) = authentication == AuthenticationToken::class.java
}