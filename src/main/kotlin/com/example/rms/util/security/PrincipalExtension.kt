package com.example.rms.util.security

import java.security.Principal

val Principal.userId
    get() = (this as AuthenticationToken).principal