package com.example.rms.util.security

import org.springframework.security.core.GrantedAuthority

enum class Authority(val grantedAuthority: GrantedAuthority) {
    USER(GrantedAuthority { "ROLE_USER" }),
    ADMIN(GrantedAuthority { "ROLE_ADMIN" })
}
