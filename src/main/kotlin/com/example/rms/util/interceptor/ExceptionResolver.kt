package com.example.rms.util.interceptor

import com.example.rms.model.dto.ApiResponse
import com.example.rms.model.exception.AbstractApiException
import com.example.rms.model.exception.common.InternalServerException
import com.example.rms.model.exception.common.MalformedRequestException
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageConversionException
import org.springframework.validation.BindException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class ExceptionResolver {
    private val logger = LoggerFactory.getLogger(ExceptionResolver::class.java)

    fun resolve(request: HttpServletRequest, response: HttpServletResponse, exception: AbstractApiException) {
        response.contentType = MediaType.APPLICATION_JSON.toString()
        response.status = exception.status().value()
        response.characterEncoding = "UTF-8"
        response.writer.write(serialize(exception))
    }

    @ExceptionHandler(value = [AbstractApiException::class])
    private fun handle(cause: AbstractApiException, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.info(cause.stackTraceToString())
        return cause.asResponse()
    }

    @ExceptionHandler(value = [ServletException::class, HttpMessageConversionException::class, BindException::class])
    private fun handle(cause: Exception, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.info(cause.stackTraceToString())
        return MalformedRequestException(cause.localizedMessage).asResponse()
    }

    @ExceptionHandler(value = [Throwable::class])
    private fun handle(cause: Throwable, request: WebRequest): ResponseEntity<ApiResponse> {
        logger.error(cause.stackTraceToString())
        return InternalServerException().asResponse()
    }

    private fun serialize(exception: AbstractApiException): String {
        val mapper = ObjectMapper().apply { findAndRegisterModules() }
        return mapper.writeValueAsString(exception)
    }
}
