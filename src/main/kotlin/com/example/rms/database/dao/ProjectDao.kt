package com.example.rms.database.dao

import com.example.rms.database.entity.Project
import com.example.rms.database.entity.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface ProjectDao : PagingAndSortingRepository<Project, Long> {
    fun findProjectById(id: Long): Project?
    fun findByNameStartsWith(query: String, pageable: Pageable): Page<Project>
    fun findByUsersContainsAndNameStartsWith(user: User, query: String, pageable: Pageable): Page<Project>
    fun findByUsersContainsAndId(user: User, id: Long): Project?
    fun existsByUsersContainsAndId(user: User, id: Long): Boolean
}