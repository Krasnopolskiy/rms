package com.example.rms.database.dao

import com.example.rms.database.entity.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface UserDao : PagingAndSortingRepository<User, Long> {
    fun findByLogin(login: String): User?
    fun findUserById(id: Long): User?
    fun findByLoginStartsWith(query: String, pageable: Pageable): Page<User>
    fun existsByLogin(login: String): Boolean
}