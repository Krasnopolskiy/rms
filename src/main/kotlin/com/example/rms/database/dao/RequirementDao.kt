package com.example.rms.database.dao

import com.example.rms.database.entity.Requirement
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface RequirementDao : PagingAndSortingRepository<Requirement, Long> {
    fun findByReleaseIdAndNameStartsWith(releaseId: Long, query: String, pageable: Pageable): Page<Requirement>
    fun findRequirementById(id: Long): Requirement?
}