package com.example.rms.database.dao

import com.example.rms.database.entity.Release
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface ReleaseDao : PagingAndSortingRepository<Release, Long> {
    fun findByProjectIdAndNameStartsWith(projectId: Long, query: String, pageable: Pageable): Page<Release>
    fun findReleaseById(id: Long): Release?
}