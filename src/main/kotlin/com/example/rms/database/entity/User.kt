package com.example.rms.database.entity

import com.example.rms.util.constant.LengthConstants
import javax.persistence.*

@Entity
class User(
    @Column(nullable = false, unique = true, length = LengthConstants.CHAR_FIELD)
    val login: String,

    @Column(nullable = false, length = LengthConstants.HASH)
    val hash: String

) : AbstractEntity() {
    @Column(nullable = false)
    var admin: Boolean = false

    @Suppress("unused") // Necessary for the correct operation of cascades
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "userId")],
        inverseJoinColumns = [JoinColumn(name = "projectId")],
        name = "Participant"
    )
    var projects: List<Project> = emptyList()
}