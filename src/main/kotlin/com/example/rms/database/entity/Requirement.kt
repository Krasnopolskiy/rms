package com.example.rms.database.entity

import com.example.rms.util.constant.LengthConstants
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

@Entity
class Requirement(
    @Column(nullable = false, length = LengthConstants.CHAR_FIELD)
    var name: String,

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "dependentId")],
        inverseJoinColumns = [JoinColumn(name = "prerequisiteId")],
        name = "Dependency"
    )
    var deps: List<Requirement> = emptyList(),

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "prerequisiteId")],
        inverseJoinColumns = [JoinColumn(name = "dependentId")],
        name = "Dependency"
    )
    var reqs: List<Requirement> = emptyList(),

    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "releaseId")
    @ManyToOne(fetch = FetchType.LAZY)
    val release: Release

) : AbstractEntity() {
    enum class State { TODO, INPROGRESS, DONE }

    @Enumerated(EnumType.STRING)
    @Column(
        nullable = false,
        columnDefinition = "ENUM( 'TODO', 'INPROGRESS', 'DONE' )"
    )
    var state: State = State.TODO
}