package com.example.rms.database.entity

import javax.persistence.*

@Entity
class Project(
    var name: String,

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "projectId")],
        inverseJoinColumns = [JoinColumn(name = "userId")],
        name = "Participant"
    )
    var users: List<User> = emptyList()

) : AbstractEntity() {

    @Suppress("unused") // Necessary for the correct operation of cascades
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = [CascadeType.ALL])
    val releases: List<Release> = emptyList()

}
