package com.example.rms.database.entity

import com.example.rms.util.constant.LengthConstants
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

@Entity
@Table(name = "ReleaseEntity") // because 'release' is a keyword for SQL
class Release(
    @Column(nullable = false, length = LengthConstants.CHAR_FIELD)
    var name: String,

    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "projectId")
    @ManyToOne(fetch = FetchType.LAZY)
    val project: Project

) : AbstractEntity() {

    @Suppress("unused") // Necessary for the correct operation of cascades
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "release", cascade = [CascadeType.ALL])
    val requrements: List<Requirement> = emptyList()

}
