package com.example.rms.handler.user

import com.example.rms.model.dto.ApiResponse
import com.example.rms.model.dto.MessageResponse
import com.example.rms.model.dto.common.DeletedMessage
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.release.ReleaseRequest
import com.example.rms.model.dto.release.ReleaseResponse
import com.example.rms.model.dto.release.ReleaseUpdatedMessage
import com.example.rms.model.dto.requirement.RequirementCreatedMessage
import com.example.rms.model.dto.requirement.RequirementRequest
import com.example.rms.model.dto.requirement.RequirementResponse
import com.example.rms.service.PermissionService
import com.example.rms.service.ReleaseService
import com.example.rms.service.RequirementService
import com.example.rms.util.security.userId
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/release"])
class ReleaseController(
    private val releaseService: ReleaseService,
    private val requirementService: RequirementService,
    private val permissionService: PermissionService
) {
    @GetMapping("/{id}")
    fun findById(principal: Principal, @PathVariable id: Long): ResponseEntity<ApiResponse> {
        permissionService.assertCanAccessRelease(principal.userId, id)
        return releaseService.findById(id).asResponse()
    }

    @PutMapping("/{id}")
    fun update(
        principal: Principal, @PathVariable id: Long, @RequestBody @Valid request: ReleaseRequest
    ): ResponseEntity<MessageResponse<ReleaseResponse>> {
        permissionService.assertCanAccessRelease(principal.userId, id)
        return ReleaseUpdatedMessage(releaseService.update(id, request)).asResponse()
    }

    @DeleteMapping("/{id}")
    fun delete(principal: Principal, @PathVariable id: Long): ResponseEntity<MessageResponse<Any>> {
        permissionService.assertCanAccessRelease(principal.userId, id)
        releaseService.delete(id)
        return DeletedMessage().asResponse()
    }

    @GetMapping("/{id}/requirement")
    fun listRequirements(
        principal: Principal,
        @PathVariable id: Long,
        @RequestParam(defaultValue = "") query: String,
        request: PageRequest
    ): ResponseEntity<ApiResponse> {
        permissionService.assertCanAccessRelease(principal.userId, id)
        return requirementService.list(id, query, request).asResponse()
    }

    @PostMapping("/{id}/requirement")
    fun create(
        principal: Principal, @PathVariable id: Long, @RequestBody @Valid request: RequirementRequest
    ): ResponseEntity<MessageResponse<RequirementResponse>> {
        permissionService.assertCanAccessRelease(principal.userId, id)
        return RequirementCreatedMessage(requirementService.create(id, request)).asResponse()
    }
}