package com.example.rms.handler.user

import com.example.rms.model.dto.MessageResponse
import com.example.rms.model.dto.auth.AuthenticatedMessage
import com.example.rms.model.dto.auth.LoginRequest
import com.example.rms.model.dto.auth.TokenResponse
import com.example.rms.service.AuthService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(path = ["/auth"])
class AuthController(
    private val service: AuthService
) {
    @PostMapping("/login")
    fun login(@RequestBody request: LoginRequest): ResponseEntity<MessageResponse<TokenResponse>> {
        return AuthenticatedMessage(service.login(request)).asResponse()
    }
}