package com.example.rms.handler.user

import com.example.rms.model.dto.ApiResponse
import com.example.rms.model.dto.MessageResponse
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.release.ReleaseCreatedMessage
import com.example.rms.model.dto.release.ReleaseRequest
import com.example.rms.model.dto.release.ReleaseResponse
import com.example.rms.service.PermissionService
import com.example.rms.service.ProjectService
import com.example.rms.service.ReleaseService
import com.example.rms.util.security.userId
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/project"])
class ProjectController(
    private val projectService: ProjectService,
    private val releaseService: ReleaseService,
    private val permissionService: PermissionService
) {
    @GetMapping
    fun list(
        principal: Principal, @RequestParam(defaultValue = "") query: String, request: PageRequest
    ): ResponseEntity<ApiResponse> {
        return projectService.listByUserId(principal.userId, query, request).asResponse()
    }

    @GetMapping("/{id}")
    fun findById(principal: Principal, @PathVariable id: Long): ResponseEntity<ApiResponse> {
        return projectService.findByUserIdAndId(principal.userId, id).asResponse()
    }

    @GetMapping("/{id}/release")
    fun listReleases(
        principal: Principal,
        @PathVariable id: Long,
        @RequestParam(defaultValue = "") query: String,
        request: PageRequest
    ): ResponseEntity<ApiResponse> {
        permissionService.assertCanAccessProject(principal.userId, id)
        return releaseService.list(id, query, request).asResponse()
    }

    @PostMapping("/{id}/release")
    fun createRelease(
        principal: Principal, @PathVariable id: Long, @RequestBody @Valid request: ReleaseRequest
    ): ResponseEntity<MessageResponse<ReleaseResponse>> {
        permissionService.assertCanAccessProject(principal.userId, id)
        return ReleaseCreatedMessage(releaseService.create(id, request)).asResponse()
    }
}