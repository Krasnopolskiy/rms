package com.example.rms.handler.user

import com.example.rms.model.dto.ApiResponse
import com.example.rms.service.UserService
import com.example.rms.util.security.userId
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping(path = ["/user"])
class UserController(
    private val service: UserService
) {
    @GetMapping("/me/profile")
    fun profile(principal: Principal): ResponseEntity<ApiResponse> {
        return service.findById(principal.userId).asResponse()
    }
}