package com.example.rms.handler.user

import com.example.rms.model.dto.ApiResponse
import com.example.rms.model.dto.MessageResponse
import com.example.rms.model.dto.common.DeletedMessage
import com.example.rms.model.dto.requirement.*
import com.example.rms.service.PermissionService
import com.example.rms.service.RequirementService
import com.example.rms.util.security.userId
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/requirement"])
class RequirementController(
    private val requirementService: RequirementService,
    private val permissionService: PermissionService
) {
    @GetMapping("/{id}")
    fun findById(principal: Principal, @PathVariable id: Long): ResponseEntity<ApiResponse> {
        permissionService.assertCanAccessRequirement(principal.userId, id)
        return requirementService.findById(id).asResponse()
    }

    @PutMapping("/{id}")
    fun update(
        principal: Principal, @PathVariable id: Long, @RequestBody @Valid request: RequirementRequest
    ): ResponseEntity<MessageResponse<RequirementResponse>> {
        permissionService.assertCanAccessRequirement(principal.userId, id)
        return RequirementUpdatedMessage(requirementService.update(id, request)).asResponse()
    }

    @PatchMapping("/{id}")
    fun update(
        principal: Principal, @PathVariable id: Long, @RequestBody @Valid request: StateRequest
    ): ResponseEntity<MessageResponse<RequirementResponse>> {
        permissionService.assertCanAccessRequirement(principal.userId, id)
        return RequirementStateUpdatedMessage(requirementService.update(id, request)).asResponse()
    }

    @DeleteMapping("/{id}")
    fun delete(principal: Principal, @PathVariable id: Long): ResponseEntity<MessageResponse<Any>> {
        permissionService.assertCanAccessRequirement(principal.userId, id)
        requirementService.delete(id)
        return DeletedMessage().asResponse()
    }
}