package com.example.rms.handler.admin

import com.example.rms.model.dto.ApiResponse
import com.example.rms.model.dto.MessageResponse
import com.example.rms.model.dto.common.DeletedMessage
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.user.UserCreatedMessage
import com.example.rms.model.dto.user.UserRequest
import com.example.rms.model.dto.user.UserResponse
import com.example.rms.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/admin/user"])
class AdminUserController(
    private val service: UserService
) {
    @GetMapping
    fun list(@RequestParam(defaultValue = "") query: String, request: PageRequest): ResponseEntity<ApiResponse> {
        return service.list(query, request).asResponse()
    }

    @PostMapping
    fun create(@RequestBody @Valid request: UserRequest): ResponseEntity<MessageResponse<UserResponse>> {
        return UserCreatedMessage(service.create(request)).asResponse()
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long): ResponseEntity<ApiResponse> {
        return service.findById(id).asResponse()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): ResponseEntity<MessageResponse<Any>> {
        service.delete(id)
        return DeletedMessage().asResponse()
    }
}