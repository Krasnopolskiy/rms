package com.example.rms.handler.admin

import com.example.rms.model.dto.ApiResponse
import com.example.rms.model.dto.MessageResponse
import com.example.rms.model.dto.common.DeletedMessage
import com.example.rms.model.dto.common.PageRequest
import com.example.rms.model.dto.project.ProjectCreatedMessage
import com.example.rms.model.dto.project.ProjectRequest
import com.example.rms.model.dto.project.ProjectResponse
import com.example.rms.model.dto.project.ProjectUpdatedMessage
import com.example.rms.service.ProjectService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/admin/project"])
class AdminProjectController(
    private val service: ProjectService
) {
    @GetMapping
    fun list(@RequestParam(defaultValue = "") query: String, request: PageRequest): ResponseEntity<ApiResponse> {
        return service.list(query, request).asResponse()
    }

    @PostMapping
    fun create(@RequestBody @Valid request: ProjectRequest): ResponseEntity<MessageResponse<ProjectResponse>> {
        return ProjectCreatedMessage(service.create(request)).asResponse()
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Long): ResponseEntity<ApiResponse> {
        return service.findById(id).asResponse()
    }

    @PutMapping("/{id}")
    fun update(@PathVariable id: Long, @RequestBody @Valid request: ProjectRequest): ResponseEntity<MessageResponse<ProjectResponse>> {
        return ProjectUpdatedMessage(service.update(id, request)).asResponse()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long): ResponseEntity<MessageResponse<Any>> {
        service.delete(id)
        return DeletedMessage().asResponse()
    }
}