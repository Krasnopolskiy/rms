# Build the Docker image with Jib
.\gradlew.bat jibDockerBuild

# Create the required networks if they don't exist
if (-not (docker network ls -q -f name=external-network)) {
    docker network create external-network
}

if (-not (docker network ls -q -f name=internal-network)) {
    docker network create internal-network
}

# Start the Docker Compose stack
docker compose -f deploy/docker-compose.yml up -d
