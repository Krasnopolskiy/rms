# Backend for Requirement Management System

## Requirements

- Docker
- Docker Compose
- JDK 11 (optional)

## TLDR

To simply build and run app, run the following command:

Linux:

```shell
./start.sh
```

Windows:

```powershell
./start.ps1
```

## Manual build and run

### Building the App Image

To build the app image, run the following command:

```shell
./gradlew jibDockerBuild
```

### Running the App

To run the stack, first create a `.env` file in the same directory as your `docker-compose.yml` file with the following
contents:

```shell
cp .env.sample .env
```

Replace variables with your preferred values.

Next, run the following command to start the stack:

```shell
docker compose -f deploy/docker-compose.yml up -d
```

This will start the `app` service and the `mysql` service, and create the required networks.

## Stopping the App

To stop the stack, run the following command:

```shell
docker compose -f deploy/docker-compose.yml down
```

This will stop and remove the containers, networks, and volumes created by the stack.
