#!/bin/bash

# Build the Docker image with Jib
./gradlew jibDockerBuild

# Create the required networks if they don't exist
if [ ! "$(docker network ls -q -f name=external-network)" ]; then
    docker network create external-network
fi

if [ ! "$(docker network ls -q -f name=internal-network)" ]; then
    docker network create internal-network
fi

# Start the Docker Compose stack
docker compose -f deploy/docker-compose.yml up -d
